# LAN MC server setup
Prerequisites: 
- Ubuntu 18.04
- [ufw]
  Helpful ufw info [from digitalocean][ufw] and [ubuntu][ufw2]
- [Spigot server][spigot]
- sudo
- sshd
- 4G RAM

Procedure:

1. Open firewall to lan: `sudo ufw allow from 192.168.1.0/24`
1. Download and confgure Spigot
   1. [download and follow install instructions][spigot-install]
   1. Download the build tools for Spigot server and build:
      - make a temp directory for building it, e.g. `~/Downloads/spigot-1.13.1`
      - download the build tools from [here][Spigot-build-tools]
      - build the version you want: `java -jar BuildTools.jar --rev 1.13.1
1. copy the resulting jars into the folder you want to keep the server files in, e.g. `/opt/spigot`
1. Create a simple start script for it, e.g. `start.sh` in the same directory as spigot:
   ```bash
#!/usr/bin/env bash
java -Xms4G -Xmx4G -jar craftbukkit-1.13.1.jar nogui

# alternatively:
# java -Xms1G -Xmx1G -XX:+UseConcMarkSweepGC -jar spigot.jar
# -XX:+UseG1GC instead of UseConcMarkSweepGC (which is deprecated since Java 9)
# add nogui to the end of the above command for the command-line only version
   ```
1. `chmod +x 
1. get the server's ip with `ip a` [more info][ip-info]
1. append the minecraft server port to that and tell your LAN friends, e.g. `192.168.1.14:25565`


## add plugins
I added [Multiverse-Core] and [Multiverse-Portals] one at a time, checking to ensure nothing broke.

> 2018 update: Multiverse-Core didn't work with the first Craftbukkit download I tried.

## make start/stop scripts
Both this script and the corresponding `minecraft_stop.sh` are inspired by StackExchange answers [here][so1] and [here][so2], as well as this [minecraft forum post][mcforum].

### start
I called mine `$HOME/minecraft_start.sh`.

```bash
#!/usr/bin/env bash
# quit if minecraft already running 
[ `screen -ls | grep minecraft | wc -l` -eq 1 ] && exit
# start the server
cd ~/mc
screen -dmS minecraft java -Xms2G -Xmx4G -XX:+UseConcMarkSweepGC -XX:ParallelGCThreads=2 -Djava.net.preferIPv4Stack=true -jar ~/mc/spigot.jar nogui
```

### stop
I called mine `$HOME/minecraft_stop.sh`.

```bash
#!/usr/bin/env bash
[ `screen -ls | grep minecraft` ] && exit
[ `screen -ls | grep minecraft | wc -l` -gt 1 ] && echo "ERROR: multiple screens named minecraft. fix it yourself!" && exit
# send minecraft server stop command and quit screen (if started by minecraft_start.sh)
# note: "^M" is Enter (write it in vim using C-v Enter)
screen -S minecraft -X stuff "stop"
```


# SSH
SSH avoids unnecessary cardio (server is downstairs).  However, lock that interface down! Use pubkeys instead of passwords:

1. `sudo apt install openssh-server`
1. make a backup of the config as explained [here][sshd-config-howto]
1. also maybe set the port to something other than 22, e.g. 44
1. (on the client) use `ssh-keygen -t rsa -b 4096 -f ~/.ssh/some_id` to make a pubkey
1. (on the client) `ssh-copy-id -i ~/.ssh/some_id -p 44 user@ip`
1. finally, disable password authentication in `/etc/sshd_config`

More info on securing ssh can be found [here][sshd-secure].

[Multiverse-Core]: https://github.com/Multiverse/Multiverse-Core
[Multiverse-Portals]: https://github.com/Multiverse/Multiverse-Portals
[so1]: https://serverfault.com/questions/178457/can-i-send-some-text-to-the-stdin-of-an-active-process-running-in-a-screen-sessi
[so1]: https://unix.stackexchange.com/questions/13953/sending-text-input-to-a-detached-screen
[mcforum]: https://www.minecraftforum.net/forums/support/server-support/server-administration/1892044-running-minecraft-in-a-screen-session
[Spigot-build-tools]: https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
[spigot-install]: https://www.spigotmc.org/wiki/spigot-installation/#linux
[spigot]: https://www.spigotmc.org/ 
[ip-info]: https://help.ubuntu.com/lts/serverguide/network-configuration.html
[ufw]: https://www.digitalocean.com/community/tutorials/how-to-setup-a-firewall-with-ufw-on-an-ubuntu-and-debian-cloud-server
[ufw2]: https://help.ubuntu.com/community/UFW
[sshd-config-howto]: https://help.ubuntu.com/lts/serverguide/openssh-server.html
[sshd-secure]: https://debian-administration.org/article/87/Keeping_SSH_access_secure
